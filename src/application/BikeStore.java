package application;
import vehicle.Bicycles;
/**
 * BikeStore
 */
public class BikeStore {
    public static void main(String[] args) {
        Bicycles[] bikeSel = new Bicycles[4];
        bikeSel[0]=new Bicycles("Trek", 20, 10);
        bikeSel[1]=new Bicycles("Steak", 20, 10);
        bikeSel[2]=new Bicycles("Shrek", 20, 10);
        bikeSel[3]=new Bicycles("Streak", 20, 10);
        
        for (Bicycles bicycles : bikeSel) {
            System.out.println(bicycles.toString());
        }
    }
    
}