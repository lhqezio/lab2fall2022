package vehicle;
/**
 * Bicycles
 */
public class Bicycles {
    private String manufacture;
    private int numberGears;
    private double maxSpeed;
    public String getManufacture() {
        return manufacture;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }
    public int getNumberGears() {
        return numberGears;
    }
    public Bicycles(String manufacture, double maxSpeed, int numberGears) {
        this.manufacture=manufacture;
        this.maxSpeed=maxSpeed;
        this.numberGears=numberGears;
    }
    @Override
    public String toString() {
        return "Manufacture: "+ manufacture + ", Number of Gears: "+ numberGears +", Max Speed: "+ maxSpeed;
    }

}